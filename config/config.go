package config

import (
	"github.com/caarlos0/env/v6"
	log "github.com/sirupsen/logrus"
)

var AppVersion = "development"

type (
	Config interface {
		GetLogLevel() string
		GetRedisDSN() string
	}

	config struct {
		LogLevel string `env:"LOG_LEVEL" envDefault:"info"`
		RedisDSN string `env:"REDIS_DSN" envDefault:"redis://127.0.0.1:6379/0"`
	}
)

func NewConfig() Config {
	cfg := &config{}
	if err := env.Parse(cfg); err != nil {
		log.Fatal(err)
	}
	return cfg
}
func (c config) GetLogLevel() string {
	return c.LogLevel
}

func (c config) GetRedisDSN() string {
	return c.RedisDSN
}
