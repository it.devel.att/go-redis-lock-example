module go-redis-lock-example

go 1.14

require (
	github.com/caarlos0/env/v6 v6.5.0
	github.com/go-redis/redis/v8 v8.8.0
	github.com/google/uuid v1.2.0
	github.com/joho/godotenv v1.3.0
	github.com/sirupsen/logrus v1.8.1
)
