FROM golang:1.14.15-alpine AS builder

RUN apk --no-cache add git gcc
WORKDIR /src
COPY . .

ARG APP_VERSION=""
RUN go build -i -ldflags "-X worker-with-lock/config.AppVersion=$APP_VERSION" -o /worker-with-lock main.go

FROM alpine:3.10

COPY --from=builder /worker-with-lock /worker-with-lock

ENTRYPOINT ["/worker-with-lock"]