PROJECT_NAME=worker-with-lock

env:
	cp .env.example .env

run-redis:
	docker-compose up -d redis_db

run:
	go run main.go

build:
	go build -o ${PROJECT_NAME} main.go

docker-build:
	docker-compose build worker-1

docker-push:
	docker-compose push

docker-up-local:
	TAG=local-demo docker-compose up -d
