package settings

import (
	"context"
	"go-redis-lock-example/config"
	"sync"
)

type (
	Shared interface {
		Context() context.Context
		Config() config.Config
		WaitGroup() *sync.WaitGroup
		Time() Clock
	}
	shared struct {
		ctx   context.Context
		cfg   config.Config
		wg    *sync.WaitGroup
		clock Clock
	}
)

func NewShared(
	ctx context.Context,
	cfg config.Config,
) Shared {
	return shared{
		ctx:   ctx,
		cfg:   cfg,
		wg:    &sync.WaitGroup{},
		clock: NewClock(),
	}
}

func (s shared) Context() context.Context {
	return s.ctx
}

func (s shared) Config() config.Config {
	return s.cfg
}

func (s shared) WaitGroup() *sync.WaitGroup {
	return s.wg
}

func (s shared) Time() Clock {
	return s.clock
}
