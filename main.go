package main

import (
	"context"
	"fmt"
	"github.com/google/uuid"
	"github.com/joho/godotenv"
	log "github.com/sirupsen/logrus"
	"go-redis-lock-example/config"
	"go-redis-lock-example/db"
	"go-redis-lock-example/settings"
	"os"
	"time"
)

func init() {
	if _, err := os.Stat(".env"); err == nil {
		if err := godotenv.Load(); err != nil {
			log.Infof("[main.init] Error while try to godotenv.Load: %v", err)
		}
	} else {
		log.Infof("[main.init] File .env not exists. Pass to load env variables from file")
	}
}

func initLogger(cfg config.Config) {
	logLevel, err := log.ParseLevel(cfg.GetLogLevel())
	if err != nil {
		log.SetLevel(log.DebugLevel)
		log.Errorf("Error while trying to parse level from config: %v", err)
	} else {
		log.SetLevel(logLevel)
	}
}

func main() {
	ctx, _ := context.WithCancel(context.Background())
	cfg := config.NewConfig()
	initLogger(cfg)

	shared := settings.NewShared(ctx, cfg)
	redisClient := db.NewRedisClient(shared)

	task := "some_task_name"
	task_name_key := fmt.Sprintf("%v:%v", "lock", task)
	workerName := uuid.New().String()

	for {
		time.Sleep(time.Second)
		success, err := redisClient.SetNX(context.Background(), task_name_key, "1", time.Minute).Result()
		if err != nil {
			log.Errorf("[%v][redisClient.SetNX] %v", workerName, err)
			time.Sleep(time.Second)
			continue
		}
		if success {
			log.Infof("[%v]Worker get job!", workerName)
			time.Sleep(time.Second * 3) // Imitate a lot of work
			_, err := redisClient.Del(context.Background(), task_name_key).Result()
			if err != nil {
				log.Errorf("[%v][redisClient.Del] %v", workerName, err)
			}
			time.Sleep(time.Second)
		}
	}
}
